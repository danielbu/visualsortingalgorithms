#include <SFML/Graphics.hpp>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>

#define WINDOW_HEIGHT 1080
#define WINDOW_WIDTH 1920
#define ARRAY_LEN 500
#define MAX_HEIGHT (WINDOW_HEIGHT * 7.f / 10.f)
#define MIN_HEIGHT (WINDOW_HEIGHT / 5.f)

//void Randomize(int* arr); //randomize the array
void DrawArray(sf::RenderWindow& window, int* arr);
void InitializeArray(int* arr);
void BubbleSort(int* arr, sf::RenderWindow& window);
void Swap(int* a, int* b);
void merge(int arr[], int l, int m, int r, sf::RenderWindow& window);
void mergeSort(int arr[], int l, int r, sf::RenderWindow& window);
int partition(int arr[], int low, int high, sf::RenderWindow& window);
void quickSort(int arr[], int low, int high, sf::RenderWindow& window);
void insertionSort(int arr[], int n, sf::RenderWindow& window);

int main() {
	srand(time(NULL));
	
	sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Sorting Algorithms");
	int arr[ARRAY_LEN] = { 0 };
	InitializeArray(arr);
	BubbleSort(arr, window);

	while (window.isOpen()) {
		sf::Event evnt;
		while (window.pollEvent(evnt)) {
			if (evnt.type == sf::Event::Closed) {
				window.close();
			}
		}

		window.clear();
		DrawArray(window, arr);
		window.display();
	}

	return 0;
}

void DrawArray(sf::RenderWindow& window, int* arr) {
	sf::RectangleShape rectangle(sf::Vector2f(0.f, 0.f));
	rectangle.setFillColor(sf::Color::White);
	for (int i = 0; i < ARRAY_LEN; i++) {
		rectangle.setSize(sf::Vector2f((float)WINDOW_WIDTH / ARRAY_LEN, -arr[i]));
		rectangle.setPosition(i * (rectangle.getSize().x), WINDOW_HEIGHT);
		window.draw(rectangle);
	}
}

void InitializeArray(int* arr) {
	for (int i = 0; i < ARRAY_LEN; i++) {
		arr[i] = (rand() % (int)MAX_HEIGHT + MIN_HEIGHT);
	}
}

void BubbleSort(int* arr, sf::RenderWindow& window) {
	for (int i = 0; i < ARRAY_LEN - 1; i++) {
		for (int j = 0; j < ARRAY_LEN - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				//DrawSwapped(arr[j], arr[j + 1], j, window);
				Swap(&arr[j], &arr[j + 1]);
				window.clear();
				DrawArray(window, arr);
				window.display();
			}		
		}
	}
}

void Swap(int* a, int* b)
{
	int t = *a;
	*a = *b;
	*b = t;
}

void merge(int arr[], int l, int m, int r, sf::RenderWindow& window)
{
	int n1 = m - l + 1;
	int n2 = r - m;

	// Create temp arrays
	int* L = new int[n1], *R = new int[n2];

	// Copy data to temp arrays L[] and R[]
	for (int i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (int j = 0; j < n2; j++)
		R[j] = arr[m + 1 + j];
	// Merge the temp arrays back into arr[l..r]
	// Initial index of first subarray
	int i = 0;
	// Initial index of second subarray
	int j = 0;
	// Initial index of merged subarray
	int k = l;

	while (i < n1 && j < n2) {
		if (L[i] <= R[j]) {
			arr[k] = L[i];
			i++;
		}
		else {
			arr[k] = R[j];
			j++;
		}
		k++;

		window.clear();
		DrawArray(window, arr);
		window.display();
	}

	// Copy the remaining elements of
	// L[], if there are any
	while (i < n1) {
		arr[k] = L[i];
		i++;
		k++;

		window.clear();
		DrawArray(window, arr);
		window.display();
	}

	// Copy the remaining elements of
	// R[], if there are any
	while (j < n2) {
		arr[k] = R[j];
		j++;
		k++;

		window.clear();
		DrawArray(window, arr);
		window.display();
	}
}

// l is for left index and r is
// right index of the sub-array
// of arr to be sorted */
void mergeSort(int arr[], int l, int r, sf::RenderWindow& window) {
	if (l >= r) {
		return;//returns recursively
	}
	int m = l + (r - l) / 2;
	mergeSort(arr, l, m, window);
	mergeSort(arr, m + 1, r, window);
	merge(arr, l, m, r, window);
}

/* This function takes last element as pivot, places
   the pivot element at its correct position in sorted
	array, and places all smaller (smaller than pivot)
   to left of pivot and all greater elements to right
   of pivot */
int partition(int arr[], int low, int high, sf::RenderWindow& window)
{
	int pivot = arr[high];    // pivot 
	int i = (low - 1);  // Index of smaller element 

	for (int j = low; j <= high - 1; j++)
	{
		// If current element is smaller than or 
		// equal to pivot 
		if (arr[j] <= pivot)
		{
			i++;    // increment index of smaller element 
			Swap(&arr[i], &arr[j]);

			window.clear();
			DrawArray(window, arr);
			window.display();
		}
	}
	Swap(&arr[i + 1], &arr[high]);
	return (i + 1);
}

/* The main function that implements QuickSort
 arr[] --> Array to be sorted,
  low  --> Starting index,
  high  --> Ending index */
void quickSort(int arr[], int low, int high, sf::RenderWindow& window)
{
	if (low < high)
	{
		/* pi is partitioning index, arr[p] is now
		   at right place */
		int pi = partition(arr, low, high, window);

		// Separately sort elements before 
		// partition and after partition 
		quickSort(arr, low, pi - 1, window);
		quickSort(arr, pi + 1, high, window);
	}
}

/* Function to sort an array using insertion sort*/
void insertionSort(int arr[], int n, sf::RenderWindow& window)
{
	int i, key, j;
	for (i = 1; i < n; i++)
	{
		key = arr[i];
		j = i - 1;

		/* Move elements of arr[0..i-1], that are
		greater than key, to one position ahead
		of their current position */
		while (j >= 0 && arr[j] > key)
		{
			arr[j + 1] = arr[j];
			j = j - 1;

			window.clear();
			DrawArray(window, arr);
			window.display();
		}
		arr[j + 1] = key;
	}
}