A sorting algorithms visualizer project.  
Built with c++ using SFML graphics library.  
A simple project without UI, destined to understand the basics of the SFML library and learn about sorting algorithms.  
  
Available sorting algorithms:  
1. Bubble sort  
2. Merge sort  
3. Quick sort  
4. Insertion sort

![](./image1.png)
